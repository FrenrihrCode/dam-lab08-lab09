import React, {Component} from 'react';
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps'
import { View, Text, Dimensions, StyleSheet } from 'react-native';
import { Avatar } from 'react-native-paper';

const width =  Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class Maps extends Component {
    constructor(props){
        super(props);
        const { restaurant } = this.props.route.params;
        this.state = {
            titulo: restaurant.title,
            focusedLocation: {
                latitude: parseFloat(restaurant.lat),
                longitude: parseFloat(restaurant.lng),
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
            }
          };
    }
    
    render() {
        return (
            <View style={styles.container}>
                <MapView
                    style={{flex: 1}}
                    provider={PROVIDER_GOOGLE}
                    initialRegion={this.state.focusedLocation} >
                                <Marker
                                    coordinate={{
                                        latitude: this.state.focusedLocation.latitude,
                                        longitude: this.state.focusedLocation.longitude
                                    }} >
                                    <Avatar.Icon size={30} icon="food" />
                                    <Callout>
                                        <View
                                            style={{
                                                backgroundColor: '#FFFFFF',
                                                borderRadius: 5
                                            }}
                                        >
                                            <Text>
                                            {this.state.titulo}
                                            </Text>
                                        </View>
                                    </Callout>
                                </Marker>
                </MapView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F2C53D',
    },
    search:{
      position: "absolute",
    },
    button: {
      backgroundColor: '#800000',
      position: "absolute",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-around",
      padding: 5,
      width:150,
      borderRadius: 10
    },
});
