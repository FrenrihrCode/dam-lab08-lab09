import React from 'react'
import { StyleSheet, View, Text, Alert } from 'react-native'
import { Button, IconButton, Card, Title, Paragraph, Dialog, Portal } from 'react-native-paper'
import firebase from '../FireBase'
import 'firebase/firestore'

function Settings(props) {
  const { route, navigation } = props
  const { restaurant } = route.params
  const {data} = restaurant
  
  const deleteDoc = () => {
      Alert.alert(
        "Eliminar Restaurant",
        "¿Está seguro de eliminar este registro?",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK", onPress: async () => {
            const db = firebase.firestore()
            await db.collection("restaurant").doc(restaurant._id).delete()
              .then( function() { navigation.navigate('Lugares') } )
              .catch(function(error) { console.error("Error removing document: ", error)})
            },
          }
        ],
        { cancelable: false }
      );
  };

  return (
    
    <View style={styles.container}>
      <Card style={{margin: 12}}>
        <Card.Content>
          <Title>
                {data.title}
          </Title>
          <Paragraph>
                {data.desc}
          </Paragraph>
          </Card.Content>
          <Card.Cover style={{padding: 8}} source={{ uri: data.image }} />
          <Card.Actions style={{justifyContent: 'space-between'}}>
            <Button mode='text' onPress={() => navigation.navigate('Map', { restaurant: data })} >
                Ver Ubicación
            </Button>
            <View style={{flexDirection: 'row'}} >
              <IconButton
                icon="pen"
                color='blue'
                size={25}
                onPress={() => navigation.navigate('Editar', { restaurant: restaurant })}
              />
              <IconButton
                icon="delete"
                color='red'
                size={25}
                onPress={deleteDoc}
              />
            </View>
          </Card.Actions>
      </Card>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#4B387A'
  },
  slogan: {
    color: '#f8f8ff',
    fontSize: 36,
    fontStyle: 'italic', backgroundColor: '#4b0082',
    textAlign: 'center', fontWeight: 'bold'
  },
  title: {
    color: '#101010',
    fontSize: 18
  },
  actorCont: { 
    flex: 1,
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,

    elevation: 16,
    backgroundColor: '#f8f8ff',
    padding: 10,
  },
  avatar: {
    flex: 1,
    backgroundColor: "#add8e6",
    borderWidth: 10,
    borderRadius: 20,
    width: 225,
    height: 250
  }
})

export default Settings