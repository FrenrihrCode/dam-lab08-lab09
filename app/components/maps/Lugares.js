import React, {useEffect} from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, SafeAreaView, FlatList, RefreshControl } from 'react-native'
import { ActivityIndicator } from 'react-native-paper'
import firebase from '../FireBase'
import 'firebase/firestore'

function Detail(props) {
  const { navigation } = props;
  const [restaurants, setRestaurants] = React.useState([]);
  const [refreshing, setRefreshing] = React.useState(false);

  useEffect(() => { 
    //getResponse();
    const unsubscribe = navigation.addListener('focus', () => {
        setRestaurants([])
        getResponse();
    });
    return () => {unsubscribe()}
  }, []);

  const getResponse = async () => {
      let col = []
      const db = firebase.firestore()
      db.settings({experimentalForceLongPolling: true})
      const data = await db.collection('restaurant').get()
      data.docs.forEach(doc => {
        col.push({_id: doc.id, data: doc.data()})
      });
      console.log("effect")
      setRestaurants(col)
  };

  const onRefresh = React.useCallback( async () => {
    setRefreshing(true)
    setRestaurants([])
    await getResponse().then(() => setRefreshing(false))
  }, [refreshing]);

  return (
    <View style={styles.contenedor}>
        <SafeAreaView >  
            <FlatList  
                data={restaurants}
                refreshControl={
                  <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }      
                renderItem={({ item }) => (
                        <View style={styles.container}>
                            <TouchableOpacity 
                              onPress={() => navigation.navigate('Detalle', { restaurant: item })}>
                            <Image
                                style={styles.tinyLogo}
                                source = {{uri: item.data.image}}
                            />
                            <View style={{flex:1, flexDirection: 'column', paddingLeft: 10}}>
                            
                                <Text style={styles.titulo}>{item.data.title}</Text>  
                                <Text style={styles.desc}>{item.data.desc}</Text>
                                                        
                            </View>
                            </TouchableOpacity>
                        </View>
                )}
                keyExtractor={item => item._id}
                ListEmptyComponent={() => (<ActivityIndicator style={{marginTop: 25}} animating={true} color='white' />)}
            />
        </SafeAreaView >
        
    </View>
  )
}

const styles = StyleSheet.create({
  container: {  
    flex: 1,
    flexDirection: 'row',
    padding: 5,
    marginHorizontal: 20,
    backgroundColor: '#e3dcf5',
    borderColor: '#5738a6',
    borderWidth: 4,
    justifyContent: 'center'
  },  
  titulo: {  
      fontWeight: 'bold',
      fontSize: 20,
      textAlign: 'center',
      marginBottom: 5
  },
  desc: {
      fontSize: 14,
      textAlign: 'center'
  },
  tinyLogo: {
      width: 120,
      height: 120,
      margin: 5,
      borderRadius: 20,
      alignSelf: 'center'
  },
  contenedor: {
    flex: 1,
    backgroundColor: '#4B387A'
  },
  text: {
    color: '#ffffff',
    fontSize: 24,
    fontWeight: 'bold'
  }
})

export default Detail