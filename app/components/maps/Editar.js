import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet } from 'react-native';
import { TextInput, Button, Snackbar } from 'react-native-paper';
import firebase from '../FireBase'
import 'firebase/firestore'

function Login(props) {
    const { route, navigation } = props
    const { restaurant } = route.params
    const {data} = restaurant
    const [titulo, setTitulo] = React.useState(data.title)
    const [desc, setDesc] = React.useState(data.desc)
    const [lat, setLat] = React.useState(data.lat)
    const [lng, setLng] = React.useState(data.lng)
    const [img, setImg] = React.useState(data.image)
    
    function agregarRes(){
      const db = firebase.firestore();
      db.settings({experimentalForceLongPolling: true})
      db.collection('restaurant').doc(restaurant._id).set({
        title: titulo,
        desc: desc,
        image: img,
        lat: lat,
        lng: lng,
      }).then(()=>{
        setTitulo('')
        setDesc('')
        setLat('')
        setLng('')
        setImg('')
        navigation.navigate('Lugares')
      }).catch((error) => {
        console.error("Error: ", error);
      });
    }

    return (
        <ScrollView style={styles.container}>
        <View style={{padding: 15}}>
          <Text style={styles.logo}>Agregar Nuevo Restaurant</Text>
            <TextInput
              style={{ marginBottom: 10 }}
              placeholder="Nombre..." 
              placeholderTextColor="white"
              value={titulo}
              onChangeText={setTitulo}/>
            <TextInput
              style={{ marginBottom: 10 }}
              placeholder="Descripcion..."
              multiline={true}
              numberOfLines={4}
              placeholderTextColor="white"
              value={desc}
              onChangeText={setDesc}/>
              <TextInput
              style={{ marginBottom: 10 }}
              placeholder="Imagen..."
              placeholderTextColor="white"
              value={img}
              onChangeText={setImg}/>
            <View style={{flexDirection: 'row', flex: 1}}>
              <TextInput
              style={{ marginBottom: 10, marginRight: 5, flex: 1 }}
              placeholder="Latitud..."
              keyboardType='phone-pad'
              placeholderTextColor="white"
              value={lat}
              onChangeText={setLat}/>
              <TextInput
              style={{ marginBottom: 10, marginLeft: 5, flex: 1 }}
              placeholder="Longitud..."
              keyboardType='phone-pad'
              placeholderTextColor="white"
              value={lng}
              onChangeText={setLng}/>
            </View>
            <Button mode='contained' onPress={agregarRes} icon="upload">Actualizar</Button>
      </View>
      </ScrollView>
    )}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#4B387A'
  },
  logo:{
    textAlign: 'center',
    fontWeight:"bold",
    fontSize:24,
    color:"white",
    marginBottom:30,
  }
});

export default Login