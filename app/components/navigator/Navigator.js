import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import TransferenceFirts from '../transferences/TransferenceFirts';
import TransferenceSecond from '../transferences/TransferenceSecond';
import TransferenceThird from '../transferences/TransferenceThird';

import MapsScreen from '../maps/Maps';
import LugaresScreen from '../maps/Lugares';
import DetallesScreen from '../maps/Detalle';
import EditarScreen from '../maps/Editar';

import HomeScreen  from '../transferences/Home';

const TransferenceStack = createStackNavigator();
function TransferenceStackScreen(){
  return(
    <TransferenceStack.Navigator>
      <TransferenceStack.Screen name='Firts' component={TransferenceFirts} options={{ headerShown: false, }} />
      <TransferenceStack.Screen name='Second' component={TransferenceSecond} 
        options={{
          title: 'Resumen de transacción',
          headerStyle: {
            backgroundColor: '#694fad',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} />
      <TransferenceStack.Screen name='Third' component={TransferenceThird}
        options={{
          title: 'Transacción realizada',
          headerLeft: null,
          headerStyle: {
            backgroundColor: '#694fad',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} />
    </TransferenceStack.Navigator>
  )
}

const MapsStack = createStackNavigator();
function MapsStackScreen(){
  return(
    <MapsStack.Navigator>
      <MapsStack.Screen name='Lugares' component={LugaresScreen} 
        options={{
          title: 'Restaurants',
          headerStyle: {
            backgroundColor: '#694fad',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} />
        <MapsStack.Screen name='Detalle' component={DetallesScreen}
        options={({ route }) => ({
            title: route.params.restaurant.data.title,
            headerStyle: {
                backgroundColor: '#694fad',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            }
        })
        } />
      <MapsStack.Screen name='Map' component={MapsScreen}
        options={{
          title: 'Ubicación',
          
          headerStyle: {
            backgroundColor: '#694fad',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} />
      <MapsStack.Screen name='Editar' component={EditarScreen}
        options={({ route }) => ({
          title: route.params.restaurant.data.title,
          headerStyle: {
              backgroundColor: '#694fad',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
              fontWeight: 'bold',
          }
        })
        } />
    </MapsStack.Navigator>
  )
}

const Tab = createMaterialBottomTabNavigator();

export default class Navigator extends Component{
  constructor(props){
    super (props);
    this.state = {
      textValue: '',
      count: 0,
    };
  }
  render(){
    return(
      <NavigationContainer>
        <Tab.Navigator
          initialRouteName="Home"
          barStyle={{ backgroundColor: '#694fad' }}
         >
          <Tab.Screen 
          name="Home" 
          component={HomeScreen}
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({color}) => (<MaterialCommunityIcons name="home" color={color} size={20} />),
          }} />
          <Tab.Screen 
          name="Transferencia" 
          component={TransferenceStackScreen}
          options={{
            tabBarLabel: 'Transferencia',
            tabBarIcon: ({color}) => (<MaterialCommunityIcons name="wallet" color={color} size={20} />),
          }} />
          <Tab.Screen 
            name="Mapa" 
            component={MapsStackScreen}
            options={{
            tabBarLabel: 'Localizacion',
            tabBarIcon: ({color}) => (<MaterialCommunityIcons name='map' color={color} size={20} />),
          }} />
          </Tab.Navigator>
      </NavigationContainer>
    );
  }
}
