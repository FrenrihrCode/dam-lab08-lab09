var moment = require('moment');
moment.locale('os');

import React, { Component } from 'react';
import { Text, View, ScrollView, TextInput, StyleSheet, Alert } from 'react-native';
import { Checkbox, Button, Appbar } from 'react-native-paper';
import ModalSelector from 'react-native-modal-selector';
import DateTimePickerModal from "react-native-modal-datetime-picker";

export default class TransferenceFirst extends Component{
    constructor(props) {
        super(props);
        let now = moment().format("DD/MM/YYYY");
        this.state = {
            cuentaOri: '',
            cuentaDes: '',
            importe: '',
            referencia: '',
            fecha: now,
            notify: false,
            isDatePickerVisible: false,
            errCuenta: '',
            errImporte: '',
            errVacio: ''
        };
    }

    validateImporte = (importe) => {
        if(importe === ''){
            this.setState({errImporte: 'Este campo debe ser llenado'})
        } else {
            var re = /^\d+(\.\d{1,2})?$/;
            if(re.test(importe)) this.setState({errImporte: ''})
            else this.setState({errImporte: 'Este campo debe tener solo números'})
        }
    };

    navegarPantalla = () => {
        if(this.state.referencia === ''){
            this.setState({errVacio: 'El campo debe ser rellenado'})
        }
        if(this.state.importe === ''){
            this.setState({errImporte: 'El campo debe ser rellenado'})
        }
        if(this.state.cuentaDes === '' || this.state.cuentaOri === ''){
            this.setState({errCuenta: 'Ambos campos deben ser llenados'})
        } else {
            if(this.state.errCuenta === '' && this.state.errImporte === '' && this.state.errVacio === '') {
                this.props.navigation.navigate('Second', {
                    cuentaOri: this.state.cuentaOri,
                    cuentaDes: this.state.cuentaDes,
                    importe: this.state.importe,
                    referencia: this.state.referencia,
                    fecha: this.state.fecha,
                    notify: this.state.notify,
                })
            }
        }
    }

    evaluarCuenta1 = (option) => {
        this.setState({cuentaOri: option.label})
        if(this.state.cuentaDes === option.label){
            this.setState({errCuenta: 'Las cuentas deben ser distintas'})
        } else {
            this.setState({errCuenta: ''})
        }
    }

    evaluarCuenta2 = (option) => {
        this.setState({cuentaDes: option.label})
        if(option.label === this.state.cuentaOri){
            this.setState({errCuenta: 'Las cuentas deben ser distintas'})
        } else {
            this.setState({errCuenta: ''})
        }
    }

    render() {
        const cuenta = [
            { key: 1, label: '0000000001234' },
            { key: 2, label: '0000000001235' },
            { key: 3, label: '0000000001236' },
            { key: 4, label: '0000000001237' },
            { key: 5, label: '0000000001238' },
            { key: 6, label: '0000000001239' },
            { key: 7, label: '0000000001240' }
        ];
        const showDatePicker = () => {
            this.setState({isDatePickerVisible:true});
        };
         
        const hideDatePicker = () => {
            this.setState({isDatePickerVisible:false});
        };
         
        const handleConfirm = (date) => {
            let fecNew = moment(date).format("DD/MM/YYYY");
            this.setState({fecha: fecNew, isDatePickerVisible:false});
        };

        return(
            <View style={styles.container}>
                <Appbar.Header style={{backgroundColor: '#6A50AD'}}>
                <Appbar.Content
                        title="Transacciones"
                        />
                    <Appbar.Action icon="send"  onPress={this.navegarPantalla} />
                </Appbar.Header>
                <ScrollView style={{paddingHorizontal: 12,
        paddingVertical:5}}>

                <Text style={{alignSelf: 'center', color:'white'}} >Cuenta origen:</Text>
                <ModalSelector
                    data={cuenta}
                    initValue="Seleccione una cuenta"
                    accessible={true}
                    onChange={(option)=>{ this.evaluarCuenta1(option) }}>
                    <TextInput
                        style={styles.text}
                        editable={false}
                        value={this.state.cuentaOri} />
                </ModalSelector>
                <Text style={{marginHorizontal: 5, color:'#f0ce75', fontSize: 12}} >{this.state.errCuenta}</Text>

                <Text style={{alignSelf: 'center', color:'white'}} >Cuenta destino:</Text>
                <ModalSelector
                    data={cuenta}
                    initValue="Seleccione una cuenta"
                    supportedOrientations={['landscape']}
                    accessible={true}
                    scrollViewAccessibilityLabel={'Scrollable options'}
                    cancelButtonAccessibilityLabel={'Cancel Button'}
                    onChange={(option)=>{ this.evaluarCuenta2(option) }}>
                    <TextInput
                        style={styles.text}
                        editable={false}
                        value={this.state.cuentaDes} />
                </ModalSelector>
                <Text style={{marginHorizontal: 5, color:'#f0ce75', fontSize: 12}} >{this.state.errCuenta}</Text>

                <Text style={{alignSelf: 'center', color:'white'}} >Importe:</Text>
                <TextInput
                    value={this.state.importe}
                    keyboardType={'numeric'}
                    style={styles.text}
                    onChangeText={ (importe) => { this.setState({ importe: importe }); 
                    this.validateImporte(importe) }}
                />
                <Text style={{marginHorizontal: 5, color:'#f0ce75', fontSize: 12}} >{this.state.errImporte}</Text>

                <Text style={{alignSelf: 'center', color:'white'}} >Referencia:</Text>
                <TextInput
                    value={this.state.referencia}
                    style={styles.text}
                    onChangeText={(referencia) => {this.setState({ referencia: referencia });
                    if(referencia === '') this.setState({errVacio: 'Este campo debe ser llenado'})
                    else this.setState({errVacio: ''})
                }}
                />
                <Text style={{marginHorizontal: 5, color:'#f0ce75', fontSize: 12}} >{this.state.errVacio}</Text>

                <Text style={{alignSelf: 'center', color:'white'}} >Fecha:</Text>
                <Button mode="contained"
                        color='white'
                        style={{marginHorizontal: 80, fontWeight: 'bold'}}
                        onPress={showDatePicker}
                >{this.state.fecha}</Button>
                <DateTimePickerModal
                    isVisible={this.state.isDatePickerVisible}
                    mode="date"
                    onConfirm={handleConfirm}
                    onCancel={hideDatePicker}
                />
                <View style={{flexDirection: 'row', justifyContent: 'center', alignContent: 'center', }}>
                    <Text style={{alignSelf: 'center', color:'white'}} >Notificar al email</Text>
                    <Checkbox
                            status={this.state.notify ? 'checked' : 'unchecked'}
                            uncheckedColor='white'
                            color='white'
                            onPress={() => { this.setState({ notify: !this.state.notify }); }}
                    />
                </View>
                

                </ScrollView>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        backgroundColor: '#4B387A'
    },
    text:{
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
        marginTop: 5,
        marginHorizontal: 5,
        backgroundColor: 'white',
        color: '#496ac4'
    },
    countContainer:{
      alignItems:'center',
      padding:10,
    },
    countText:{
      color: '#FF00FF',
    },
    top: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0
      }
  });