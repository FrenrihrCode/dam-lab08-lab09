var moment = require('moment');
moment.locale('os');

import React, { Component } from 'react';
import { Text, View, TextInput, ScrollView, StyleSheet } from 'react-native';
import { Button } from 'react-native-paper';

export default class TransferenceSecond extends Component{
    constructor(props) {
        super(props);
        this.state = {
            cuentaOri: this.props.route.params.cuentaOri,
            cuentaDes: this.props.route.params.cuentaDes,
            importe: this.props.route.params.importe,
            referencia: this.props.route.params.referencia,
            fecha: this.props.route.params.fecha,
            notify: this.props.route.params.notify,
        };
    }
    
    render() {
        return(
            <View style={styles.container}>
                <ScrollView>

                <Text style={{color:'white', marginTop:2, fontWeight: 'bold'}} >Cuenta origen:</Text>
                <Text style={styles.text} >{this.state.cuentaOri}</Text>

                <Text style={{color:'white', fontWeight: 'bold'}} >Cuenta destino:</Text>
                <Text style={styles.text} >{this.state.cuentaDes}</Text>

                <Text style={{color:'white', fontWeight: 'bold'}} >Importe:</Text>
                <Text style={styles.text} >{this.state.importe}</Text>

                <Text style={{color:'white', fontWeight: 'bold'}} >Referencia:</Text>
                <Text style={styles.text} >{this.state.referencia}</Text>

                <Text style={{color:'white', fontWeight: 'bold'}} >Fecha:</Text>
                <Text style={styles.text} >{this.state.fecha}</Text>
                
                <Text style={{ color:'white', fontWeight: 'bold'}} >Recibir notificación:</Text>
                <Text style={styles.text} >{this.state.notify ? "SI" : "NO"}</Text>
                <View style={{ flexDirection:'row', justifyContent: 'space-around', marginTop: 10, marginHorizontal: 12}}>
                
                <Button mode="contained"
                    color='#db1616'
                    onPress={() => { this.props.navigation.goBack() }}>Cancelar</Button>
                <Button mode="contained"
                    color='#08a63d'
                    onPress={() => { this.props.navigation.navigate('Third') }}>Confirmar envio</Button>

                </View>
                
                </ScrollView>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        backgroundColor: '#4B387A',
        paddingHorizontal: 12,
        paddingVertical:5
    },
    text:{
        fontWeight: 'bold',
        fontSize: 16,
        backgroundColor: '#ebe3ff',
        color: '#496ac4',
        marginBottom: 5,
        padding: 9
    },
    countContainer:{
      alignItems:'center',
      padding:10,
    },
    countText:{
      color: '#FF00FF',
    },
  });