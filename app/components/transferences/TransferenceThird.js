var moment = require('moment');
moment.locale('os');

import React, { Component } from 'react';
import { View } from 'react-native';
import { Avatar, Button } from 'react-native-paper';

export default class TransferenceThird extends Component{
    constructor(props) {
        super(props);
        this.state = {
            importe: 0,
        };
    }
    render() {
        return(
            <View style={{flex: 1, alignContent: 'center', justifyContent: 'center', backgroundColor: '#4B387A'}}>
                <Avatar.Icon size={120} icon="check" style={{backgroundColor: 'green', alignSelf: 'center'}} />
                <Button 
                    style={{marginHorizontal: 50, marginTop: 20}}
                    mode='contained' color='white'  onPress={() => { this.props.navigation.popToTop() }}>ACEPTAR</Button>
            </View>
        )
    }
}