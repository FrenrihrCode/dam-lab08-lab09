import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet } from 'react-native';
import { TextInput, Button, Snackbar } from 'react-native-paper';
import firebase from '../FireBase'
import 'firebase/firestore'

function Login(props) {
    const [titulo, setTitulo] = React.useState('');
    const [desc, setDesc] = React.useState('');
    const [lat, setLat] = React.useState('');
    const [lng, setLng] = React.useState('');
    const [img, setImg] = React.useState('');
    const [visible, setVisible] = React.useState(false);
    const { navigation } = props;
    
    function agregarRes(){
      const db = firebase.firestore();
      db.settings({experimentalForceLongPolling: true})
      db.collection('restaurant').add({
        title: titulo,
        desc: desc,
        image: img,
        lat: lat,
        lng: lng,
      }).then(()=>{
        setVisible(!visible)
        setTitulo('')
        setDesc('')
        setLat('')
        setLng('')
        setImg('')
      })
    }


    return (
        <ScrollView style={styles.container}>
        <View style={{padding: 15}}>
          <Text style={styles.logo}>Agregar Nuevo Restaurant</Text>
            <TextInput
              style={{ marginBottom: 10 }}
              placeholder="Nombre..." 
              placeholderTextColor="white"
              value={titulo}
              onChangeText={setTitulo}/>
            <TextInput
              style={{ marginBottom: 10 }}
              placeholder="Descripcion..."
              multiline={true}
              numberOfLines={4}
              placeholderTextColor="white"
              value={desc}
              onChangeText={setDesc}/>
              <TextInput
              style={{ marginBottom: 10 }}
              placeholder="Imagen..."
              placeholderTextColor="white"
              value={img}
              onChangeText={setImg}/>
            <View style={{flexDirection: 'row', flex: 1}}>
              <TextInput
              style={{ marginBottom: 10, marginRight: 5, flex: 1 }}
              placeholder="Latitud..."
              keyboardType='phone-pad'
              placeholderTextColor="white"
              value={lat}
              onChangeText={setLat}/>
              <TextInput
              style={{ marginBottom: 10, marginLeft: 5, flex: 1 }}
              placeholder="Longitud..."
              keyboardType='phone-pad'
              placeholderTextColor="white"
              value={lng}
              onChangeText={setLng}/>
            </View>
              <Button mode='contained' onPress={agregarRes} icon="upload">Agregar</Button>
              <Snackbar
                visible={visible}
                onDismiss={() => setVisible(false)}
                action={{
                  label: 'Ver',
                  onPress: () => {
                    navigation.navigate('Mapa')
                  },
                }} >Restaurant añadido
              </Snackbar>
      </View>
      </ScrollView>
    )}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#4B387A'
  },
  logo:{
    textAlign: 'center',
    fontWeight:"bold",
    fontSize:24,
    color:"white",
    marginBottom:30,
  }
});

export default Login