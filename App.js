import Navigator from './app/components/navigator/Navigator';
import React, { Component } from 'react';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      textValue: 0
    };
  }
  render(){
    return <Navigator />;
  }
} 